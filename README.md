# MySQL Footbal League system database
## Generic info:
This repository contains solutions to a series of practical assignments focused on database design and SQL querying. Each assignment builds upon the previous one, covering topics such as database schema design, CRUD operations, subqueries, stored procedures, transactions, and views.
## Description of db structure: 
A football league system is a hierarchy of leagues that compete in the same sport. Each league has a number of teams that play against each other in a round-robin format. The teams are ranked by points, goal difference, and goals scored. The top teams of each league may be promoted to a higher league, while the bottom teams may be relegated to a lower league.  
![diagram](pa1/diagram.png)
- **players**: Stores information about players such as name, birth date, nationality, position, and jersey number.  
- **teams**: Contains details about football teams including name, city, stadium, and manager.  
- **team_squads**: Represents the many-to-many relationship between teams and players.   
- **seasons**: Stores information about football seasons.  
- **leagues**: Contains details about football leagues.  
- **match_days**: Represents match days within a season and league.  
- **matches**: Stores information about football matches including date/time, venue, teams, and scores.  
- **goals**: Contains details about goals scored in matches.  
- **standings**: Stores the current standings of teams in each match day.
- **tournament_standing**: This view summarizes the standing of players in a tournament based on the total goals scored by each player in each match day.
- **goal_distribution_by_time_interval**: This view analyzes the distribution of goals scored across different time intervals within a match.
- **sample_data**: This table contains random sample data.  
- **sample_data_clone**: This table is a clone of the sample_data table with indexes applied to demonstrate the difference between indexed and non-indexed search.  
## Practical assignments content:
### Assignment 1
**Description:** In this assignment, we designed a relational database for a football league system. The database stores information about teams, players, matches, goals, seasons, leagues, match days, and standings. Additionally, three simple queries were written for each table, covering clauses such as WHERE, GROUP BY, HAVING, ORDER BY, and LIMIT, etc.

**Bonus Task:** 
- The bonus task involved writing a query prepared by the teacher, which may include advanced SQL concepts or scenarios not covered in the basic assignment. 

### Assignment 2
**Description:** This assignment added relationships between tables, including primary and foreign keys, and table constraints. Many-to-many relationships were established, and additional SQL queries including many-to-many relationships were added.

**Bonus Task:**
- Learnt about indexes and applied simple indexes to demonstrate the difference between indexed and non-indexed search.

### Assignment 3
**Description:** Assignment 3 focused on practicing SQL subqueries and sets. Select, update, and delete queries were written for various clauses, including non-correlated and correlated subqueries. Queries involving UNION, UNION ALL, INTERSECT, and EXCEPT were also implemented.

**Bonus Task:**
- Developed a simple Python application to communicate with the MySQL database, executing CRUD operations.

### Assignment 4
**Description:** This assignment involved preparing stored procedures to work with different types of parameters (IN, OUT, INOUT). A transactional mechanism was added with conditional logic to commit or rollback changes. Stored procedure invocations were demonstrated, and different transaction outcomes were observed.

**Bonus Task:**
- Demonstrated knowledge of working with transactions using the application layer by implementing simple CRUD operations.

### Assignment 5
**Description:** For this assignment, two different views were created to select the standing table with the latest data and distribution of goals scored across different time intervals within a match.

**Bonus Task:**
-  GitLab integration was demonstrated by registering an account, creating a repository, and pushing data before and after the assignment.

## Description of repo structure: 
- **pa1/**
    - `schema.sql`: Contains schema associated queries
    - `queries.sql`: Contains CRUD operations only
    - `bonus.sql`: Contains a simple bonus query

- **pa2/**
    - `queries.sql`: Contains standard queries with many-to-many relationships
    - **bonus/**
      - `bonus.py`: Contains a Python script for generation of table with random data
      - `bonus.sql`: Contains SQL queries with non-indexed and indexed search

- **pa3/**
    - `subqueries.sql`: Contains queries involving subqueries
    - **bonus/**
      - `bonus.py`: Contains a Python application, executing CRUD operations
      - `execution_screenshots`:  Contains execution screenshots 

- **pa4/**
    - `procedures.sql`: Contains stored procedures
    - `executions.sql`: Contains procedure invocations
    - `bonus.py`: Contains a Python application, executing a transaction

- **pa5/**
    - `views.sql`: Contains view creation queries
    - `bonus.MD`: Information describing itLab integration bonus task

- **research/**
    - `presentation`: Contains research presentation  
## How to deploy the project
To deploy the project, follow these steps:
1. Clone the repository to your local machine using `git clone`.
2. Set up a MySQL server or use an existing one.
3. Execute the SQL schema files located in the `pa1/` directory.
4. Run the SQL query files containing CRUD operations to populate the database with sample data.
5. Run the SQL query files containing other operations (stored procedures, transaction,s etc.)
6. If bonus tasks involve additional steps, follow the instructions below.

## How to run bonus tasks 
Follow these steps to run bonus tasks:
1. Navigate to the directory containing the bonus task files (e.g., `bonus/`, `bonus.py`).
2. Read any accompanying documentation or instructions provided within the bonus task files.
3. Execute the necessary scripts or commands to run the bonus tasks. Use IDE supporting language bonus tasks are written in.
4. Verify the output or results to ensure the bonus tasks are executed successfully.

## Info about author
The project is authored by `Ira Velmakina`. For any inquiries or assistance related to the project, you can reach out to the author via ivelmakina@kse.org.ua. 
