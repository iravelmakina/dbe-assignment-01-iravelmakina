-- Select standings for tournament 1
CREATE VIEW tournament_standing AS
SELECT player_name,
       team_name,
       SUM(total_goals) AS total_goals,
       GROUP_CONCAT(match_days_with_goals SEPARATOR ', ') AS match_days_with_goals
FROM (
         SELECT players.name AS player_name,
                teams.name AS team_name,
                matches.match_day_id,
                COUNT(goals.id) AS total_goals,
                CONCAT(matches.match_day_id, '(', COUNT(goals.id), ')') AS match_days_with_goals
         FROM players
                  JOIN goals ON players.id = goals.player_id
                  JOIN teams ON goals.team_id = teams.id
                  JOIN matches ON goals.match_id = matches.id
         GROUP BY players.name, teams.name, matches.match_day_id
     ) AS subquery
GROUP BY player_name, team_name;

-- Analyze the distribution of goals scored across different time intervals within a match
CREATE VIEW goal_distribution_by_time_interval AS
SELECT
    matches.id AS match_id,
    matches.date_time AS match_date_time,
    CASE
        WHEN goals.goal_time <= '00:45:00' THEN 'First Half'
        WHEN goals.goal_time <= '01:20:00' THEN 'Second time'
        ELSE 'Extra time'
        END AS time_interval,
    COUNT(*) AS goals_scored
FROM
    matches
        JOIN goals ON matches.id = goals.match_id
GROUP BY
    matches.id,
    time_interval;

select * from tournament_standing;
select * from goal_distribution_by_time_interval;
