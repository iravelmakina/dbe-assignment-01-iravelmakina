# Publishing Changes to Different Repositories
To demonstrate proficiency in working with different remotes for the same repository, follow these steps:

1. **Register on GitLab**: Sign up for an account on GitLab if you haven't already.

2. **Create a Repository**: Create a new repository on GitLab with the same name as your local repository. Ensure that it is empty.

3. **Add GitLab Remote**: In your local repository, add the GitLab repository as a remote.

4. **Push Data Before PA5**: Before starting PA5, push your existing data to the GitLab repository:

5. **Work on PA5**: Complete your work for PA5 as usual, committing changes locally.

6. **Push Changes for PA5**: Once you have completed your work for PA5, push the changes to both GitHub and GitLab repositories:

7. **Create a Merge Request**: On GitLab, navigate to your repository and create a new merge request, comparing the changes between the master branch on GitHub and the master branch on GitLab. Provide a brief description of the changes and submit the merge request.

8. **Review and Merge**: Review the merge request on GitLab, ensuring that the changes are correct. If everything looks good, merge the changes into the master branch on GitLab.

By following these steps, you can effectively demonstrate your ability to work with different remotes for the same repository and publish changes to multiple repositories on different platforms like GitHub and GitLab
