-- SELECT -- 
# = with non-correlated subqueries result
-- Find players from Manchester United
SELECT name
FROM players
WHERE id IN (
    SELECT player_id
    FROM team_squads
    WHERE team_id = (
        SELECT id
        FROM teams
        WHERE name = 'Manchester United'
    )
);

# IN with non-correlated subqueries result
-- Find match venue and date when at least one player from Spain has scored a goal:
SELECT venue AS match_venue, DATE(date_time) AS match_date
FROM matches
WHERE id IN (
    SELECT match_id
    FROM goals
    WHERE player_id IN (
        SELECT id
        FROM players
        WHERE nationality = 'Spain'
    )
);

# NOT IN with non-correlated subqueries result
-- Find teams that have not won any matches yet.
SELECT name
FROM teams
WHERE id NOT IN (
    SELECT team_id
    FROM standings
    WHERE won > 0
);

# EXISTS with non-correlated subqueries result
-- Retrieve players who belong to teams where manager's name starts from 'M'.
SELECT name
FROM players
WHERE EXISTS (
    SELECT 1
    FROM teams
    WHERE manager LIKE 'M%'
);

# NOT EXISTS with non-correlated subqueries result
-- Find positions of players who are not associated with a team that has a stadium named "Anfield".
SELECT position
FROM players
WHERE NOT EXISTS (
    SELECT 1
    FROM teams
    WHERE stadium = 'Anfield'
);

# = with correlated subqueries result
-- Find players from Spain who have scored goals for their respective teams. 
SELECT name
FROM players 
WHERE players.id = (
    SELECT player_id
    FROM goals
    WHERE player_id = players.id AND players.nationality = 'Spain' 
    LIMIT 1
);

# IN with correlated subqueries result
-- Find information about teams that have participated in matches where they played as the 
-- home team and won, and whose names start with letters ranging from 'F' to 'P'.
SELECT name, city, stadium
FROM teams 
WHERE teams.id IN (
    SELECT matches.home_team_id
    FROM matches 
    WHERE matches.home_score > matches.away_score AND teams.name REGEXP '^[F-P]'
);

# NOT IN with correlated subqueries result
-- Find teams that have not played in a specific stadium (including their home stadium).
SELECT name
FROM teams
WHERE teams.id NOT IN (
    SELECT DISTINCT home_team_id
    FROM matches
    WHERE venue = 'Anfield' OR teams.stadium = 'Anfield'
    UNION
    SELECT DISTINCT away_team_id
    FROM matches
    WHERE venue = 'Old Trafford' OR teams.stadium = 'Old Trafford'
);

# EXISTS with correlated subqueries result
-- Find teams that have ever been in a position (based on their overall performance) where they lost a match by more than 2 goals
SELECT teams.name AS team_name
FROM teams
WHERE EXISTS (
    SELECT 1
    FROM standings
    WHERE standings.team_id = teams.id
      AND (standings.goals_against - standings.goals_for) > 2
);

# NOT EXISTS with correlated subqueries result
-- Find teams that have not scored any goals in any of their matches.
SELECT teams.name AS team_name
FROM teams 
WHERE NOT EXISTS (
    SELECT 1
    FROM matches 
    WHERE matches.home_team_id = teams.id AND matches.home_score > 0
       OR matches.away_team_id = teams.id AND matches.away_score > 0
);

-- UPDATE -- 
# = with non-correlated subqueries result
-- Update the manager for all teams in 'English Premier League' league
UPDATE teams
SET manager = 'Jake Swan'
WHERE id = (
    SELECT team_id
    FROM standings
    ORDER BY points DESC
    LIMIT 1
);

# IN with non-correlated subqueries result
-- Update the position of players who have not scored any goals to be 'Substitute'.
ALTER TABLE players
    MODIFY COLUMN position ENUM('Goalkeeper', 'Defender', 'Midfielder', 'Forward', 'Substitute') NOT NULL;

UPDATE players
SET position = 'Substitute'
WHERE id NOT IN (
    SELECT DISTINCT player_id
    FROM goals
);

# NOT IN with non-correlated subqueries result
-- Update the season names that do not have any match days to be 'Upcoming Season'.
UPDATE seasons
SET name = 'Upcoming Season'
WHERE id NOT IN (
    SELECT DISTINCT season_id
    FROM match_days
);

# EXISTS with non-correlated subqueries result
-- Update the manager of any team that has Cristiano Ronaldo as a player to 'Sir Alex Ferguson'.
UPDATE teams
SET manager = 'Sir Alex Ferguson'
WHERE EXISTS (
    SELECT *
    FROM players
    WHERE players.name = 'Cristiano Ronaldo'
);

# NOT EXISTS with non-correlated subqueries result
-- Update date_time to '2023-08-15 15:00:00' for matches where all stadiums in the teams table are not equal to 'Old Trafford'.
UPDATE matches
SET date_time = '2023-08-15 15:00:00'
WHERE NOT EXISTS (
    SELECT *
    FROM teams
    WHERE teams.stadium != 'Old Trafford'
);

# = with correlated subqueries result
-- Update the manager of the team with the fewest points (less than 4) to 'Tom Black'.
UPDATE teams 
SET manager = 'Tom Black'
WHERE teams.id = (
    SELECT team_squads.team_id
    FROM team_squads 
             INNER JOIN standings 
                 ON team_squads.team_id = standings.team_id
    WHERE standings.points < 4
    ORDER BY standings.points
    LIMIT 1
);

# IN with correlated subqueries result
-- Update the manager for teams that have at least one player born before 1990 to 'Senior Manager'.
UPDATE teams 
SET manager = 'Senior Manager'
WHERE teams.id IN (
    SELECT team_squads.team_id
    FROM team_squads
    WHERE team_squads.player_id IN (
        SELECT players.id
        FROM players
        WHERE YEAR(players.birth_date) < 1990
          AND team_squads.team_id = teams.id
    )
);

# NOT IN with correlated subqueries result
-- Update the position of players to 'Away Player' if they do not belong to any team located in a specific city.
ALTER TABLE players
    MODIFY COLUMN position ENUM('Goalkeeper', 'Defender', 'Midfielder', 'Forward', 'Substitute', 'Away Player') NOT NULL;

UPDATE players 
SET position = 'Away Player'
WHERE players.id NOT IN (
    SELECT team_squads.player_id
    FROM team_squads 
             INNER JOIN teams ON team_squads.team_id = teams.id
    WHERE teams.city = 'Manchester'
);

# EXISTS with correlated subqueries result
-- Increment the points in the standings table by 3 for each team that has won at least one match. 
UPDATE standings
SET points = points + 3
WHERE EXISTS (
    SELECT 1
    FROM matches 
    WHERE (matches.home_team_id = standings.team_id AND matches.home_score > matches.away_score)
       OR (matches.away_team_id = standings.team_id AND matches.away_score > matches.home_score)
);

# NOT EXISTS with correlated subqueries result
-- Update a position of players Substitute 
-- if there are no records in the 'goals' table associated with that player.
UPDATE players
SET position = 'Substitute'
WHERE NOT EXISTS (
    SELECT 1
    FROM goals
    WHERE goals.player_id = players.id
);


-- DELETE -- 
# = with non-correlated subqueries result
-- Remove players from the players table who have scored goals before the time '00:15:00'
DELETE 
FROM players
WHERE id = (
    SELECT player_id 
    FROM goals 
    WHERE goal_time < '00:15:00');

# IN with non-correlated subqueries result
-- Delete players who have scored exactly two goals in a match.
DELETE
FROM players
WHERE id IN (
    SELECT player_id
    FROM goals
    GROUP BY player_id
    HAVING COUNT(*) = 2
);

# NOT IN with non-correlated subqueries result
-- Delete teams with a winless record.
DELETE 
FROM teams
WHERE id NOT IN (
    SELECT team_id 
    FROM standings
    WHERE standings.won = 0
);

# EXISTS with correlated subqueries result
-- DELETE teams that have ever been in a position (based on their overall performance) where they lost a match by more than 3 goals
DELETE
FROM teams
WHERE EXISTS (
    SELECT 1
    FROM standings
    WHERE standings.team_id = teams.id
      AND (standings.goals_against - standings.goals_for) > 3
);

# NOT EXISTS with correlated subqueries result
-- DELETE teams that have not scored any goals in any of their matches.
DELETE
FROM teams
WHERE NOT EXISTS (
    SELECT 1
    FROM matches
    WHERE matches.home_team_id = teams.id AND matches.home_score > 0
       OR matches.away_team_id = teams.id AND matches.away_score > 0
);
    
# = with correlated subqueries result
-- DELETE players from Spain who have scored goals for their respective teams. 
SELECT name
FROM players
WHERE players.id = (
    SELECT player_id
    FROM goals
    WHERE player_id = players.id AND players.nationality = 'Spain'
    LIMIT 1
);

# IN with correlated subqueries result
-- DELETE teams from the 'teams' table if they have participated as a home team in matches where their score exceeded 
-- the opponent's score and if their name starts with a letter between F and P.
DELETE
FROM teams
WHERE teams.id IN (
    SELECT matches.home_team_id
    FROM matches
    WHERE matches.home_score > matches.away_score AND teams.name REGEXP '^[F-P]'
);
    
# NOT IN with correlated subqueries result
-- Delete player records from the 'players' table if they have not scored any goals or if their jersey number is not '10'.
DELETE 
FROM players 
WHERE players.id NOT IN (
    SELECT DISTINCT player_id
    FROM goals
    
    UNION
    SELECT players.id
    WHERE players.number = '10'
);
    
# EXISTS with correlated subqueries result
-- Delete teams that have at least one player from Norway.
DELETE 
FROM teams 
WHERE EXISTS (
    SELECT * 
    FROM team_squads 
    WHERE teams.id = team_squads.team_id
    AND player_id IN (
            SELECT id FROM players 
            WHERE nationality = 'Norway')
);

# NOT EXISTS with correlated subqueries result
-- Delete teams that do not have any player who plays as a goalkeeper.
DELETE 
FROM teams 
WHERE NOT EXISTS (
    SELECT * 
    FROM team_squads 
    WHERE teams.id = team_squads.team_id
    AND EXISTS (
        SELECT * 
        FROM players 
        WHERE team_squads.player_id = players.id AND position = 'Goalkeeper')
);


-- ---------------------------------------------------------------------------------------------------------------------------------------------
# four SELECT queries that include the clause UNION / UNION ALL / INTERSECT / EXCEPT (could be separate queries or include multiple set clauses).
-- UNION -- 
-- Retrieve the names of players and teams participating in the league
SELECT name AS participant_name, 'Player' AS participant_type
FROM players
UNION
SELECT name AS participant_name, 'Team' AS participant_type
FROM teams;

-- UNION ALL -- 
-- Retrieve the names of players and teams participating in the league (with duplicates)
SELECT stadium, 'Team_stadium' AS type
FROM teams
UNION ALL
SELECT venue AS stdium, 'Match_venue' AS type
FROM matches;

-- INTERSECT -- 
-- Find players who are also from Spain and forwards
SELECT name
FROM players
WHERE nationality = 'Argentina'
INTERSECT
SELECT name
FROM players
WHERE position = 'Forward';

-- EXCEPT (NOT IN or LEFT JOIN) -- 
-- Select names of teams where there are no players with the position 'Midfielder' assigned to that team.
SELECT name
FROM teams
WHERE id NOT IN (
    SELECT teams.id
    FROM teams 
             INNER JOIN team_squads ON teams.id = team_squads.team_id
             INNER JOIN players ON team_squads.player_id = players.id
    WHERE players.position = 'Midfielder'
);
