import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    port=3307,
    password="my-secret-pw",
    database="football_league"
)

cursor = mydb.cursor()


# Create operation
def create_team(name, city, stadium, manager):
    sql = "INSERT INTO teams (name, city, stadium, manager) VALUES (%s, %s, %s, %s)"
    val = (name, city, stadium, manager)
    cursor.execute(sql, val)
    mydb.commit()
    print(cursor.rowcount, "record inserted.")


# Read operation
def read_teams():
    sql = "SELECT * FROM teams WHERE stadium LIKE %s"
    val = ("%Stadium%",)
    cursor.execute(sql, val)
    result = cursor.fetchall()
    for row in result:
        print(row)


# Update operation
def update_team_manager(manager, city):
    sql = "UPDATE teams SET manager = %s WHERE city = %s"
    val = (manager, city)
    cursor.execute(sql, val)
    mydb.commit()
    print(cursor.rowcount, "record(s) affected.")


# Delete operation
def delete_team(team_id):
    sql = "DELETE FROM teams WHERE id = %s"
    val = (team_id, )
    cursor.execute(sql, val)
    mydb.commit()
    print(cursor.rowcount, "record(s) deleted.")


create_team("Chicago Bears", "Chicago", "Guaranteed Rate Field", "Tom Young")
read_teams()
update_team_manager("Ethan Presley", "Milan")
delete_team("11")

cursor.close()
mydb.close()
