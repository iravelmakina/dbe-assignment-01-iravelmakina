-- Find teams with specified location and manager conditions, ordered by team name in descending order.
SELECT name AS team_name, city, manager AS manager_name
FROM team
WHERE city != 'Sanbao' OR manager LIKE '%n'
ORDER BY team_name DESC;

-- Find distinct team cities, concatenating team name and uppercased city, excluding records with NULL city value
SELECT concat(name, ' : ', UPPER(city)) AS team_cities
FROM team
WHERE city IN ('Mukun', 'Xiangride');

-- Find teams with stadium name ending with "stadium", limited to the first record and ordered by desc.
SELECT *
FROM team
WHERE stadium LIKE '%Stadium'
ORDER BY stadium DESC
LIMIT 1;

-- Find number of players grouped by their playing positions.
SELECT position, COUNT(*) AS players_number
FROM player
GROUP BY position
HAVING players_number BETWEEN 6 AND 10;

-- Find the minimum age among goalkeepers or players with names starting with 'A'.
SELECT MIN(YEAR(CURRENT_DATE()) - YEAR(birth_date)) AS minimum_age
FROM player
WHERE position = 'Goalkeeper' OR name LIKE '__a%';

-- Find players number by position with names starting from A to D.
SELECT position, COUNT(*) AS number_of_players
FROM player
WHERE name REGEXP '^[A-D]'
GROUP BY position
ORDER BY number_of_players;

-- Find the names of matches and their total home scores, filtering matches played between the 4th and 14th days of the month. Results are ordered by the sum of home scores and limited to 8 records, starting from the 3rd record.
SELECT CONCAT(id, ' — ', SUM(home_score)) AS home_team_score
FROM matches
WHERE EXTRACT(DAY FROM date_time) BETWEEN 4 AND 8
GROUP BY id
ORDER BY SUM(home_score)
LIMIT 5
    OFFSET 2;

-- Find the rounded average home and away scores for each venue, excluding venues where either average is zero.
SELECT venue, ROUND(AVG(home_score)) AS average_home_score, ROUND(AVG(away_score)) AS average_away_score
FROM matches
GROUP BY venue
HAVING average_home_score > 0 AND average_away_score > 0
ORDER BY venue;

-- Find information about the latest home match of Madlin Rimour, including match number, date, and time.
SELECT matches.id AS `match`, date_time
FROM matches
         INNER JOIN team
                    ON matches.home_team_id = team.id
WHERE team.name = 'Madlin Rimour'
ORDER BY DATE(date_time) DESC
LIMIT 1;

-- Find matches where goals were scored before 25th minute.
SELECT match_id AS `match`, UPPER(team.name) AS team_name, goal_time
FROM goal
         INNER JOIN team
                    ON goal.team_id = team.id
WHERE (TIME_TO_SEC(goal_time) / 60) < 25
ORDER BY goal_time, match_id;

-- Find teams which scored less than 2 goals during league.
SELECT team.name AS team, COUNT(*) AS goals_scored
FROM goal
         INNER JOIN team
                    ON goal.team_id = team.id
GROUP BY team.id
HAVING goals_scored < 8
ORDER BY goals_scored DESC;

-- Find teams with a second position in Premier Ligue:
SELECT team.name AS team, league.name AS league, position
FROM standing
         INNER JOIN team
                    ON standing.team_id = team.id
         INNER JOIN league
                    ON standing.league_id = league.id
WHERE league.name = 'Premier League'
  AND team.name IN (
    SELECT team.name
    FROM standing
             INNER JOIN team
                        ON standing.team_id = team.id
    WHERE league.name = 'Premier League' AND position = 2
);

-- Find teams' goal differences and order by them:
SELECT team.name AS team, league.name AS league, ABS(goals_against - goals_for) AS goal_difference
FROM standing
         INNER JOIN team ON standing.team_id = team.id
         INNER JOIN league ON standing.league_id = league.id
WHERE league.name = 'Premier League'
ORDER BY goal_difference;

-- Find teams with the most wins in the league:
SELECT team.name AS team, won AS wins
FROM standing
         INNER JOIN league
                    ON standing.league_id = league.id
         INNER JOIN team
                    ON standing.team_id = team.id
WHERE won = (
    SELECT MAX(won)
    FROM standing
) and league.name = 'Premier League';
