USE football_league;
CREATE TABLE players (
                         id INTEGER PRIMARY KEY AUTO_INCREMENT,
                         name VARCHAR(255) NOT NULL,
                         birth_date DATE,
                         nationality VARCHAR(255),
                         position ENUM('Goalkeeper', 'Defender', 'Midfielder', 'Forward') NOT NULL,
                         number INTEGER NOT NULL
);

CREATE TABLE teams (
                       id INTEGER PRIMARY KEY AUTO_INCREMENT,
                       name VARCHAR(255) NOT NULL,
                       city VARCHAR(255) NOT NULL,
                       stadium VARCHAR(255) NOT NULL,
                       manager VARCHAR(255)
);

CREATE TABLE team_squads (
                             team_id INTEGER,
                             player_id INTEGER,
                             PRIMARY KEY (team_id, player_id),
                             FOREIGN KEY (team_id) REFERENCES teams(id),
                             FOREIGN KEY (player_id) REFERENCES players(id)
);

CREATE TABLE seasons (
                         id INTEGER PRIMARY KEY AUTO_INCREMENT,
                         name VARCHAR(255)
);

CREATE TABLE leagues (
                         id INTEGER PRIMARY KEY AUTO_INCREMENT,
                         name VARCHAR(255) NOT NULL,
                         number_of_teams INTEGER
);

CREATE TABLE match_days (
                            id INTEGER PRIMARY KEY AUTO_INCREMENT,
                            season_id INTEGER NOT NULL,
                            league_id INTEGER NOT NULL,
                            day_number INTEGER NOT NULL CHECK (day_number > 0),
                            CONSTRAINT season2league2day_unique UNIQUE (season_id, league_id, day_number),
                            FOREIGN KEY (season_id) REFERENCES seasons(id),
                            FOREIGN KEY (league_id) REFERENCES leagues(id)
);

CREATE TABLE matches (
                         id INTEGER PRIMARY KEY AUTO_INCREMENT,
                         date_time DATETIME NOT NULL,
                         venue VARCHAR(255) NOT NULL,
                         home_team_id INTEGER,
                         away_team_id INTEGER,
                         home_score INTEGER NOT NULL,
                         away_score INTEGER NOT NULL,
                         match_day_id INTEGER,
                         FOREIGN KEY (home_team_id) REFERENCES teams(id),
                         FOREIGN KEY (away_team_id) REFERENCES teams(id),
                         FOREIGN KEY (match_day_id) REFERENCES match_days(id)
);

CREATE TABLE standings (
                           match_day_id INTEGER NOT NULL,
                           team_id INTEGER NOT NULL,
                           points INTEGER NOT NULL,
                           played INTEGER NOT NULL,
                           won INTEGER NOT NULL,
                           drawn INTEGER NOT NULL,
                           lost INTEGER NOT NULL,
                           goals_for INTEGER NOT NULL,
                           goals_against INTEGER NOT NULL,
                           PRIMARY KEY (match_day_id, team_id),
                           FOREIGN KEY (team_id) REFERENCES teams(id),
                           FOREIGN KEY (match_day_id) REFERENCES match_days(id)
);

CREATE TABLE goals (
                       id INTEGER PRIMARY KEY AUTO_INCREMENT,
                       match_id INTEGER,
                       player_id INTEGER,
                       team_id INTEGER,
                       goal_time TIME NOT NULL,
                       FOREIGN KEY (match_id) REFERENCES matches(id),
                       FOREIGN KEY (player_id) REFERENCES players(id),
                       FOREIGN KEY (team_id) REFERENCES teams(id)
);

INSERT INTO teams (name, city, stadium, manager)
VALUES
    ('Manchester United', 'Manchester', 'Old Trafford', 'Ole Gunnar Solskjaer'),
    ('Liverpool FC', 'Liverpool', 'Anfield', 'Jurgen Klopp'),
    ('FC Barcelona', 'Barcelona', 'Camp Nou', 'Ronald Koeman'),
    ('Real Madrid CF', 'Madrid', 'Santiago Bernabeu', 'Carlo Ancelotti'),
    ('Juventus FC', 'Turin', 'Allianz Stadium', 'Massimiliano Allegri'),
    ('Paris Saint-Germain', 'Paris', 'Parc des Princes', 'Mauricio Pochettino'),
    ('Bayern Munich', 'Munich', 'Allianz Arena', 'Julian Nagelsmann'),
    ('Manchester City', 'Manchester', 'Etihad Stadium', 'Pep Guardiola'),
    ('AC Milan', 'Milan', 'San Siro', 'Stefano Pioli'),
    ('Borussia Dortmund', 'Dortmund', 'Signal Iduna Park', 'Marco Rose');

-- Players table
INSERT INTO players (name, birth_date, nationality, position, number)
VALUES
    ('David De Gea', '1990-11-07', 'Spain', 'Goalkeeper', 1),
    ('Virgil van Dijk', '1991-07-08', 'Netherlands', 'Defender', 4),
    ('Lionel Messi', '1987-06-24', 'Argentina', 'Forward', 10),
    ('Sergio Ramos', '1986-03-30', 'Spain', 'Defender', 4),
    ('Cristiano Ronaldo', '1985-02-05', 'Portugal', 'Forward', 7),
    ('Neymar Jr', '1992-02-05', 'Brazil', 'Forward', 10),
    ('Robert Lewandowski', '1988-08-21', 'Poland', 'Forward', 9),
    ('Kevin De Bruyne', '1991-06-28', 'Belgium', 'Midfielder', 17),
    ('Zlatan Ibrahimovic', '1981-10-03', 'Sweden', 'Forward', 11),
    ('Erling Haaland', '2000-07-21', 'Norway', 'Forward', 9);

-- Team_Player (Team_Squad) table
INSERT INTO team_squads (team_id, player_id)
VALUES
    (1, 1), (1, 2), (1, 3), (1, 4), (1, 5),
    (2, 2), (2, 6), (2, 7), (2, 8),
    (3, 3), (3, 9), (3, 10),
    (4, 4), (4, 5), (4, 6),
    (5, 5), (5, 7), (5, 8),
    (6, 6), (6, 9), (6, 10),
    (7, 7), (7, 8), (7, 9),
    (8, 1), (8, 3), (8, 5),
    (9, 9), (9, 10),
    (10, 1), (10, 4), (10, 6);

-- Seasons table
INSERT INTO seasons (name)
VALUES
    ('2023-2024'),
    ('2024-2025');

-- Leagues table
INSERT INTO leagues (name, number_of_teams)
VALUES
    ('English Premier League', 10),
    ('La Liga', 10),
    ('Bundesliga', 10),
    ('Serie A', 10);

-- Match_Days table
INSERT INTO match_days (season_id, league_id, day_number)
VALUES
    (1, 1, 1), (1, 1, 2), (1, 1, 3),
    (1, 2, 1), (1, 2, 2), (1, 2, 3),
    (1, 3, 1), (1, 3, 2), (1, 3, 3),
    (1, 4, 1), (1, 4, 2), (1, 4, 3);

-- Matches table
INSERT INTO matches (date_time, venue, home_team_id, away_team_id, home_score, away_score, match_day_id)
VALUES
    ('2023-08-15 15:00:00', 'Old Trafford', 1, 2, 2, 1, 1),
    ('2023-08-22 16:30:00', 'Anfield', 2, 1, 3, 0, 1),
    ('2023-08-29 15:00:00', 'Camp Nou', 3, 4, 1, 2, 1),
    ('2023-08-15 15:00:00', 'Santiago Bernabeu', 4, 3, 2, 2, 2),
    ('2023-08-22 16:30:00', 'Allianz Stadium', 5, 6, 1, 1, 2),
    ('2023-08-29 15:00:00', 'Parc des Princes', 6, 5, 2, 1, 2),
    ('2023-08-15 15:00:00', 'Allianz Arena', 7, 8, 3, 0, 3),
    ('2023-08-22 16:30:00', 'Etihad Stadium', 8, 7, 0, 2, 3),
    ('2023-08-29 15:00:00', 'San Siro', 9, 10, 2, 2, 3),
    ('2023-08-15 15:00:00', 'Signal Iduna Park', 10, 9, 1, 3, 4),
    ('2023-08-22 16:30:00', 'Old Trafford', 1, 3, 2, 1, 4),
    ('2023-08-29 15:00:00', 'Anfield', 2, 4, 1, 1, 4);

INSERT INTO goals (match_id, player_id, team_id, goal_time)
VALUES
    (1, 1, 1, '00:15:00'), (1, 2, 2, '00:30:00'),
    (3, 3, 3, '00:25:00'), (4, 4, 4, '00:10:00'),
    (5, 5, 5, '00:20:00'), (5, 5, 5, '00:21:00'),
    (6, 6, 6, '00:10:00'), (7, 7, 7, '00:05:00'),
    (8, 8, 8, '00:18:00'), (9, 9, 9, '00:12:00'),
    (10, 10, 10, '00:22:00'), (11, 1, 1, '00:28:00'),
    (12, 2, 2, '00:35:00');

-- Standings table
INSERT INTO standings (match_day_id, team_id, points, played, won, drawn, lost, goals_for, goals_against)
VALUES
    (1, 1, 3, 1, 1, 0, 0, 2, 1),
    (1, 2, 0, 1, 0, 0, 1, 1, 2),
    (1, 3, 3, 1, 1, 0, 0, 1, 2),
    (1, 4, 1, 1, 0, 1, 0, 2, 2),
    (1, 5, 1, 1, 0, 1, 0, 1, 1),
    (1, 6, 3, 1, 1, 0, 0, 2, 1),
    (1, 7, 3, 1, 1, 0, 0, 3, 0),
    (1, 8, 0, 1, 0, 0, 1, 0, 2),
    (1, 9, 1, 1, 0, 1, 0, 2, 2),
    (1, 10, 0, 1, 0, 0, 1, 1, 3),
    (2, 1, 4, 2, 2, 0, 0, 5, 1),
    (2, 2, 1, 2, 0, 1, 1, 3, 3),
    (2, 3, 4, 2, 2, 0, 0, 3, 2),
    (2, 4, 2, 2, 0, 2, 0, 4, 4),
    (2, 5, 2, 2, 0, 2, 0, 2, 2),
    (2, 6, 3, 2, 1, 0, 1, 3, 2),
    (2, 7, 4, 2, 2, 0, 0, 6, 0),
    (2, 8, 0, 2, 0, 0, 2, 0, 5),
    (2, 9, 2, 2, 0, 2, 0, 4, 4),
    (2, 10, 1, 2, 0, 1, 1, 3, 4),
    (3, 1, 7, 3, 3, 0, 0, 10, 2),
    (3, 2, 1, 3, 0, 1, 2, 4, 6),
    (3, 3, 7, 3, 3, 0, 0, 4, 2),
    (3, 4, 4, 3, 1, 1, 1, 6, 6),
    (3, 5, 2, 3, 0, 2, 1, 3, 3),
    (3, 6, 4, 3, 1, 1, 1, 5, 4),
    (3, 7, 6, 3, 3, 0, 0, 9, 0),
    (3, 8, 0, 3, 0, 0, 3, 0, 8),
    (3, 9, 3, 3, 1, 0, 2, 6, 6),
    (3, 10, 2, 3, 0, 2, 1, 5, 7),
    (4, 1, 7, 4, 3, 1, 0, 12, 3),
    (4, 2, 2, 4, 0, 2, 2, 5, 8),
    (4, 3, 7, 4, 3, 1, 0, 6, 3),
    (4, 4, 5, 4, 1, 2, 1, 8, 8),
    (4, 5, 3, 4, 0, 3, 1, 5, 5),
    (4, 6, 4, 4, 1, 1, 2, 7, 6),
    (4, 7, 9, 4, 4, 0, 0, 13, 0),
    (4, 8, 1, 4, 0, 1, 3, 3, 10),
    (4, 9, 4, 4, 1, 1, 2, 8, 10),
    (4, 10, 3, 4, 0, 3, 1, 8, 11);
