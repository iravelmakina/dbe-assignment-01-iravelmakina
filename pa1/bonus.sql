-- Find players with more than 1,5 goals scored
SELECT player.id, player.name, FLOOR(AVG(goals_count)) AS average_goals_scored
FROM player
         INNER JOIN (
    SELECT player_id, COUNT(id) AS goals_count
    FROM goal
    GROUP BY player_id
) AS player_goals ON player.id = player_goals.player_id
GROUP BY player.id, player.name
HAVING AVG(goals_count) > 1.5
ORDER BY average_goals_scored DESC;
