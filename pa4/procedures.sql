-- Stored procedures --
-- IN/OUT -- 
-- Get all the goals a certain team scored in all the matches.
CREATE PROCEDURE get_team_goals(
    IN team_name VARCHAR(255),
    OUT total_goals INT
)
BEGIN
    SELECT COUNT(*) INTO total_goals
    FROM goals 
    JOIN teams 
    ON goals.team_id = teams.id
    WHERE teams.name = team_name;
END;

-- INOUT -- 
-- Get a modified team name (team name, city)
CREATE PROCEDURE update_team_city(
    INOUT team_name VARCHAR(255)
)
BEGIN
    DECLARE team_city VARCHAR(255);

    SELECT city INTO team_city
    FROM teams
    WHERE name = team_name;

    SET team_name = CONCAT(team_name, ', ', team_city);
END;

SET @team_name = 'AC Milan';

-- Transactions -- 
CREATE PROCEDURE update_player_position_conditional(
    IN player_id INT,
    IN new_position ENUM('Goalkeeper', 'Defender', 'Midfielder', 'Forward')
)
BEGIN
    DECLARE old_position ENUM('Goalkeeper', 'Defender', 'Midfielder', 'Forward');
    DECLARE player_goalkeeper BOOLEAN;

    START TRANSACTION;
    SELECT position INTO old_position
    FROM players
    WHERE id = player_id;
    
    SELECT CASE WHEN position = 'Goalkeeper' THEN TRUE ELSE FALSE END INTO player_goalkeeper
    FROM players
    WHERE id = player_id;
    IF old_position <> new_position AND player_goalkeeper = FALSE THEN
        UPDATE players
        SET position = new_position
        WHERE id = player_id;

        COMMIT;

        SELECT CONCAT('Player ', player_id, ' position updated from ', old_position, ' to ', new_position) AS Result;
    ELSE
        ROLLBACK;
        SELECT 'Player position update failed. Either the new position is the same as the current position or the player is a goalkeeper. Some other errors possible.' AS Result;
    END IF;
END;

