import mysql.connector


def connect_to_database():
    return mysql.connector.connect(
        host="localhost",
        port='3307',
        user="root",
        password="my-secret-pw",
        database="football_league"
    )


def perform_transaction(operations):
    connection = connect_to_database()
    cursor = connection.cursor()

    try:
        connection.start_transaction()

        for operation, values in operations:
            operation(cursor, values)

        connection.commit()
        print("Transaction committed!")

    except mysql.connector.Error as error:
        print("Transaction failed. Rolling back changes.")
        connection.rollback()
        print("Error:", error)

    finally:
        cursor.close()
        connection.close()


def insert_player(cursor, values):
    cursor.execute("INSERT INTO players (name, birth_date, nationality, position, number) VALUES (%s, %s, %s, %s, %s)",
                   values)


def find_player(cursor, value):
    cursor.execute("SELECT name FROM players WHERE nationality = %s", (value,))
    result = cursor.fetchall()
    return result


def update_player(cursor, values):
    cursor.execute("UPDATE players SET position = %s WHERE name = %s", values)


def delete_player(cursor, value):
    cursor.execute("DELETE FROM players WHERE id = %s", (value,))


operations = [
    (insert_player, ("David Hilton", "1995-05-15", "USA", "Midfielder", 10)),
    (find_player, "USA"),
    (update_player, ("Substitute", "David Hilton")),
    (delete_player, 11)
]

perform_transaction(operations)
