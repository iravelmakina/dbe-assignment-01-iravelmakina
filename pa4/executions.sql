CALL get_team_goals('Manchester United', @goals);
SELECT @goals AS total_goals;

CALL update_team_city(@team_name);
SELECT @team_name AS modified_team_name;

-- Call the stored procedure with valid input, where the new position is different from the old position and the player is not a goalkeeper.
CALL update_player_position_conditional(3, 'Midfielder');

-- Call the stored procedure with valid input, where the new position is the same as the old position.
CALL update_player_position_conditional(5, 'Forward');

-- Call the stored procedure with invalid input, where the player is a goalkeeper.
CALL update_player_position_conditional(1, 'Defender');

-- Call the stored procedure with invalid input, where the player ID does not exist.
CALL update_player_position_conditional(100, 'Forward');
