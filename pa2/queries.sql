--  Queries including many-to-many relationships -- 

-- Retrieve all players in a specific team whose age is no more than 37 years:
SELECT players.id AS player_id, players.name AS player_name, players.birth_date, players.nationality, players.position, players.number
FROM players
         JOIN team_squads
              ON players.id = team_squads.player_id
         JOIN teams
              ON team_squads.team_id = teams.id
WHERE teams.name = 'Manchester United' AND YEAR(CURRENT_DATE()) - YEAR(birth_date) <= 36
ORDER BY player_name;

-- Retrieve all teams and their cities a specific player is a part of with the number of goals they scored:
SELECT teams.name, teams.city, COUNT(goals.id) AS goals_scored
FROM teams
         JOIN team_squads ON teams.id = team_squads.team_id
         JOIN players ON team_squads.player_id = players.id
         LEFT JOIN goals ON teams.id = goals.team_id
WHERE players.name = 'Neymar Jr'
GROUP BY teams.id;

-- Find teams which scored less than 3 goals in English Premier League
SELECT name
FROM teams
WHERE id IN (
    SELECT team_id
    FROM team_squads
    WHERE player_id IN (
        SELECT id
        FROM players
        WHERE id IN (
            SELECT player_id
            FROM goals
            WHERE match_id IN (
                SELECT id
                FROM matches
                WHERE match_day_id IN (
                    SELECT id
                    FROM match_days
                    WHERE league_id IN (
                        SELECT id
                        FROM leagues
                        WHERE name = 'English Premier League'
                    )
                )
            )
        )
    )
    GROUP BY team_id
    HAVING COUNT(id) < 3
);
