CREATE TABLE sample_data_clone AS
SELECT * FROM sample_data;

-- ~14 - avg number of identical names 
SELECT AVG(name_count) AS average_name_count
FROM (
         SELECT name, COUNT(*) AS name_count
         FROM sample_data_clone
         GROUP BY name
     ) AS name_counts;


-- ~131 - avg number of identical cities
SELECT AVG(city_count) AS average_city_count
FROM (
         SELECT city, COUNT(*) AS city_count
         FROM sample_data_clone
         GROUP BY city
     ) AS city_counts;


-- that is why it is better to create such a composite index
CREATE INDEX idx_name_city ON sample_data_clone (name, city);

-- number of rows in each table
SELECT COUNT(*)
FROM sample_data;

SELECT COUNT(*)
FROM sample_data_clone;


-- non-indexed search
SELECT *
FROM sample_data
WHERE city = 'Kirbyfort' AND name = 'Jodi Alvarado';

-- indexed search
SELECT *
FROM sample_data_clone
WHERE city = 'Kirbyfort' AND name = 'Jodi Alvarado';
