## Script for data generation ##
import mysql.connector
from faker import Faker
import random

conn = mysql.connector.connect(
    host="localhost",
    port=3307,
    user="root",
    password="my-secret-pw",
    database="football_league"
)

cursor = conn.cursor()

create_table_query = """
CREATE TABLE IF NOT EXISTS sample_data (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    age INT,
    email VARCHAR(255),
    city VARCHAR(255)
)
"""
cursor.execute(create_table_query)

fake = Faker()
commit_interval = 100000  
for i in range(30_000_000):
    name = fake.name()
    age = random.randint(18, 99)
    email = fake.email()
    city = fake.city()

    insert_query = "INSERT INTO sample_data (name, age, email, city) VALUES (%s, %s, %s, %s)"
    cursor.execute(insert_query, (name, age, email, city))

    if i % commit_interval == 0:
        conn.commit()

conn.commit()
conn.close()

print("Data generation and insertion complete.")
